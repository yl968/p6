# Project p6

Project p6 is a Rust-based AWS Lambda function designed to interact with AWS services, including X-Ray for monitoring and traceability. It leverages asynchronous programming to handle Lambda events efficiently and is configured to utilize environment-based logging for easier debugging and monitoring.

## Features

- **AWS Lambda Integration**: Built to run as a serverless function in AWS Lambda, providing scalable and on-demand processing capabilities.
- **CloudWatch Logs Integration**: Configured to log output to AWS CloudWatch, enabling easy monitoring and logging of Lambda function execution.
- **X-Ray Tracing**: Supports AWS X-Ray for tracing requests through the AWS infrastructure, aiding in performance and issue diagnostics.

## Dependencies

- `lambda_runtime`: For AWS Lambda function interface.
- `serde`, `serde_json`: For serialization and deserialization of JSON data.
- `log`, `simple_logger`, `tracing`, `tracing-subscriber`: For logging and tracing functionalities.
- `aws-sdk-cloudwatchlogs`, `aws-config`, `aws-sdk-xray`: AWS SDKs for CloudWatch Logs and X-Ray integration.
- `ring`, `rusoto_core`, `rusoto_dynamodb`: For security and AWS service interactions, particularly DynamoDB.
- `tokio`: Runtime for asynchronous programming.
- `openssl`: For secure operations, with vendored support.
- `env_logger`: Environment-based logger configuration.

## Setup and Deployment

1. **Prerequisites**:
   - Install Rust and Cargo.
2. **Compile**:
   - Compile the project for the `x86_64-unknown-linux-musl` target to ensure compatibility with AWS Lambda's execution environment.
   ```bash
   cargo build --release --target x86_64-unknown-linux-musl
   ```
3. **Logging implementation**:
   - Add this line to the Rust function
   ```bash
   log::info!("Received event: {:?}", event); // Log the received event
   ```
   - Add this line to main function
   ```bash
   env_logger::Builder::from_default_env()
        .filter(None, LevelFilter::Info)
        .init();
   ```
4. **Dependencies**:
   - Add dependencies
   ```bash
   [dependencies]
    lambda_runtime = "0.4.1"
    serde = { version = "1.0", features = ["derive"] }
    serde_json = "1.0"
    log = "0.4"
    simple_logger = "1.0"
    tracing = "0.1"
    tracing-subscriber = "0.2"
    aws-sdk-cloudwatchlogs = "0.2.0" 
    aws-config = { version = "1.1.7", features = ["behavior-version-latest"] }
    aws-sdk-xray = "1.15.0"
    ring = "0.17.8"
    rusoto_core = "0.47.0"
    rusoto_dynamodb = "0.47.0"
    tokio = { version = "1", features = ["full"] }
    openssl = { version = "0.10", features = ["vendored"] }
    env_logger = "0.9"
    ```
5. **Deployment Package**:
   - Create a deployment package by zipping the compiled binary named `bootstrap`.
   ```bash
   zip lambda.zip ./target/x86_64-unknown-linux-musl/release/bootstrap
   ```
6. **Deploy to AWS Lambda**:
   - Use the AWS CLI or AWS Management Console to create or update the Lambda function with the deployment package.


## Logging and Monitoring

- **CloudWatch**: Logs are automatically sent to CloudWatch under the `/aws/lambda/<function-name>` log group.
![Image](8810030b7b1f8b80c6eafe2d6c7b6eb.png)
- **X-Ray**: Ensure X-Ray tracing is enabled for the Lambda function for detailed tracing information.
![Image](9a94e73991b7f67255c8525a56cf0b6.png)
