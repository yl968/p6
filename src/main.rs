use lambda_runtime::{handler_fn, Context, Error};
use rusoto_core::Region;
use rusoto_dynamodb::{DynamoDb, DynamoDbClient, ListTablesInput};
use serde_json::{Value, json};
use log::{self, LevelFilter};
use env_logger;

async fn function_handler(event: Value, _: Context) -> Result<Value, Error> {
    log::info!("Received event: {:?}", event); // Log the received event
    let client = DynamoDbClient::new(Region::UsEast1);
    match client.list_tables(ListTablesInput::default()).await {
        Ok(output) => {
            let tables = output.table_names.unwrap_or_else(|| vec!["No tables found".to_string()]);
            Ok(json!({ "tables": tables }))
        },
        Err(e) => {
            log::error!("Error querying DynamoDB: {:?}", e); // Log errors querying DynamoDB
            Err(e.into())
        },
    }
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    env_logger::Builder::from_default_env()
        .filter(None, LevelFilter::Info)
        .init();

    let func = handler_fn(function_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}